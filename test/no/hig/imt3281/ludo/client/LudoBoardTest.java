package no.hig.imt3281.ludo.client;

import static org.junit.Assert.*;
import no.hig.imt3281.ludo.client.LudoBoard.LudoLogic;

import org.junit.Test;

public class LudoBoardTest {

	@Test
	public void testCanMoveTo() {
		LudoBoard.LudoLogic board = new LudoLogic();
		assertEquals(0, board.playerPosToBoardPos(LudoBoard.RED, 4),0);
		assertEquals(43, board.playerPosToBoardPos(LudoBoard.GREEN, 8),0);
		assertEquals(20, board.playerPosToBoardPos(LudoBoard.YELLOW, 50),0);
		assertEquals(5, board.playerPosToBoardPos(LudoBoard.BLUE, 48),0);
		
		assertTrue(board.canMove(LudoBoard.RED, -1, 4));	// Can move out of starting grid
		board.movePiece(LudoBoard.GREEN, -1, 17);
		assertEquals(17, board.getPosition(LudoBoard.GREEN, 0),0);
		board.movePiece(LudoBoard.GREEN, -1, 17);
		assertEquals(2, board.opponentsCount(LudoBoard.RED, 4), 0);
		assertFalse(board.canMove(LudoBoard.RED, -1, 4));		// Blocked by green, can not move out
		assertFalse(board.canMove(LudoBoard.GREEN, -1, 5)); 	// Can not move from starting grid and anywhere but starting position
		assertTrue(board.canMove(LudoBoard.RED, 5, 9));			// Can move piece if not blocked
		assertFalse(board.canMove(LudoBoard.RED, 55, 58));	// Can not move past block
		board.movePiece(LudoBoard.GREEN, 17, 20);
		board.movePiece(LudoBoard.GREEN, 17, 19);
		assertEquals(1, board.opponentsCount(LudoBoard.RED, 6), 0);
		assertEquals(1, board.opponentsCount(LudoBoard.RED, 7), 0);
		assertTrue(board.canMove(LudoBoard.RED, 4, 9));			// No block, can move
		assertFalse(board.canMove(LudoBoard.RED, 4, 11));		// Can not move 11
		assertFalse(board.canMove(LudoBoard.RED, 60, 63));	// Can not move past goal
		
		assertTrue(board.canUse(LudoBoard.RED, 6));				// A six on the dice moves you out from home
		assertFalse(board.canUse(LudoBoard.RED, 5));				// Need a six of higher to move out from home
		board.movePiece(LudoBoard.RED, -1, 20);
		assertTrue(board.canUse(LudoBoard.RED, 3));				// One piece in play, can move with anything
		board.movePiece(LudoBoard.RED, 20, 60);
		assertFalse(board.canUse(LudoBoard.RED, 3));				// One piece on play, only 2 to complete
		assertTrue(board.canUse(LudoBoard.RED, 2));				// Gets one piece to goal
		assertTrue(board.canUse(LudoBoard.RED, 6));				// Can get another piece out from home;
		
		//fail("Not yet implemented");
	}

}
