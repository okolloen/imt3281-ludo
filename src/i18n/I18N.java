package i18n;

import java.util.ResourceBundle;

/**
 * Class used to simplify accessing the i18n resource bundles
 * 
 * @author oivindk
 *
 */
public class I18N {
	private static ResourceBundle server;
	private static ResourceBundle client;

	/**
	 * Get the i18n bundle for the server.
	 * 
	 * @return a ResourceBundle containing i18n for the server application
	 */
	public static ResourceBundle getServerBundle () {
		if (server == null) {
			server = ResourceBundle.getBundle("i18n.server");
		}
		return server;
	}
	
	/**
	 * Get the i18n bundle for the client.
	 * 
	 * @return a ResourceBundle containing i18n for the client application
	 */
	public static ResourceBundle getClientBundle () {
		if (client == null) {
			client = ResourceBundle.getBundle("i18n.client");
		}
		return client;
	}
}