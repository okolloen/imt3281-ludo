package no.hig.imt3281.ludo.client;

import i18n.I18N;






import java.awt.BorderLayout;
import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;






import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextPane;






import no.hig.imt3281.ludo.Logging;
import no.hig.imt3281.ludo.util.MD5;

@SuppressWarnings("serial")
public class Client extends JFrame {
	private final static ResourceBundle resources = I18N.getClientBundle();
	private final static Logger logger = Logging.getLogger();
	private final static int loginPortNumber = 8989;
	private final static int masterPortNumber = 8990;
	private final static int channelsRequestPortNumber = 8988;
	private static Preferences prefs;
	private static String serverName; 
	private static Socket connection = null;
	private static BufferedReader input = null;
	private static PrintWriter output = null;
	private final static String sslPwd = "P@ssw0rd!";
	private final static String sslFName = "/config/ssl.key";
	private JDesktopPane desktop = new JDesktopPane();
	private Chat globalChat;
	private HashMap<Long, Game> games = new HashMap<>();
	
	public Client (String uname, String pwd) {
		super ("Ludo");
		prefs = Preferences.userNodeForPackage(getClass());
		setSize (1100, 1000);
		setJMenuBar(createMenuBar());
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		if (prefs.get("uname", "").equals("")) {
			showIntroInfo();		// Also does register new user
		} else {
			add (desktop);
			if (uname==null) {
				if (autoLogin(prefs.get("uname", ""), prefs.get("password", ""))) {
					validate();
					openGlobalChat();
					startListener();
				}
			} else {
				if (autoLogin(uname, pwd)) {
					validate();
					openGlobalChat();
					startListener();
				}				
			}
		}
	}

	private void openGlobalChat() {
		Logging.getLogger().info("Opening global chat rom");
		globalChat = new Chat(-1, resources.getString("globalchat.windowTitle"), output);
		globalChat.setSize (600, 500);
		desktop.add(globalChat);
		globalChat.setVisible(true);
	}

	private boolean autoLogin(String uname, String pwd) {
		serverName = prefs.get("serverName", "127.0.0.1");
		try {
			loginConnect();
			if (output==null)
				throw new IOException ("Not connected");
			output.printf("LOGIN\t%s\t%s\n", uname, pwd);
			output.flush();
			String tmp = input.readLine();
			input.close();
			output.close();
			connection.close();
			Logging.getLogger().info(tmp);
			if (tmp.startsWith("LOGIN OK")) {
				connection = new Socket (serverName, masterPortNumber);
				output = new PrintWriter(new OutputStreamWriter(connection.getOutputStream()));
				input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				Logging.getLogger().info("Using un-encrypted communications");
				String key = tmp.split("[\t]")[1];
				output.println(key);
				output.flush();
				String response = input.readLine();
				Logging.getLogger().info(response);
				setTitle("Ludo - "+uname);
				return true;
			} else if (tmp.equals("Uknown username/password")) {
				JOptionPane.showMessageDialog(this, resources.getString("unknownUsernamePassword"));
			} else 
				JOptionPane.showMessageDialog(this, resources.getString("unableToLogIn"));
		} catch (UnknownHostException e) {
			Logging.printStackTrace(e);
			JOptionPane.showMessageDialog(this, resources.getString("unknownServerError"));
		} catch (IOException e) {
			Logging.printStackTrace(e);
			JOptionPane.showMessageDialog(this, resources.getString("serverCommunicationsError"));
		}
		return false;
	}

	private void showIntroInfo() {
		JTextPane intro = new JTextPane();
		intro.setContentType("text/html");
		try {
			intro.setPage(getClass().getResource("/resources/intro.html"));
		} catch (IOException e) {	// This shouldn't ever happen
			Logging.getLogger().info("Unable to read intro.html");
		}
		add (intro);
		JButton ok = new JButton ("Confirm");
		JPanel p = new JPanel ();
		p.add(ok);
		ok.addActionListener(e -> {
			remove (intro);
			remove (p);
			add (desktop);
			UserInfoEntryWindow uiew = new UserInfoEntryWindow ();
			uiew.pack();
			desktop.add (uiew);
			uiew.setVisible(true);
			uiew.getConnect().addActionListener(e1 -> {
				if (checkNewUserDetails (uiew)) {
					if (createNewUser (uiew)) {
						desktop.remove (uiew);
						setJMenuBar(createMenuBar());
						desktop.validate();
						desktop.repaint();
						repaint();
						openGlobalChat();
						startListener();
					};
				}
			});
		});
		add (p, BorderLayout.SOUTH);
	}

	private void startListener() {
		new Thread (()->{
			while (input!=null) {
				String cmd;
				try {
					while ((cmd=input.readLine())!=null) {
						String command[] = cmd.split("[\t]");
						if (command[0].equals("MESSAGE")) {
							if (command[1].equals("-1")) {
								globalChat.addMessage(command[2], command[3], false);
							}
						} else if (command[0].equals("NEW GAME")) {
							startNewGame (command);
						} else if (command[0].equals("PLAYER TURN")) {
							games.get(Long.parseLong(command[1])).showActivePlayer(Integer.parseInt(command[2]));
							if (command.length>3&&command[3].equals("YOUR TURN"))
								games.get(Long.parseLong(command[1])).myTurn(Integer.parseInt(command[2]));
						} else if (command[0].equals("MOVE")) {
							games.get(Long.parseLong(command[1])).movePiece(Integer.parseInt(command[2]), Integer.parseInt(command[3]), Integer.parseInt(command[4]));
						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();		
	}

	private boolean createNewUser(UserInfoEntryWindow uiew) {
		try {
			loginConnect ();
			if (output==null) {
				throw new IOException ("Not connected");
			}
			output.printf  ("REGISTER\t%s\t%s\t%s\t%s\t%s\n", uiew.getEmail().getText(), 
					MD5.MD5(uiew.getPassword().getText()), uiew.getGivenname().getText(), 
					uiew.getSurename().getText(), uiew.getNickname().getText());
			output.flush ();
			String tmp = input.readLine();
			if (tmp.equals("REGISTER OK")) {
				prefs.put("uname", uiew.getEmail().getText());
				prefs.put("password", MD5.MD5(uiew.getPassword().getText()));
				prefs.put("nickname", uiew.getNickname().getText());
				prefs.put("serverName", uiew.getServerName().getText());
				prefs.put("givenName", uiew.getGivenname().getText());
				prefs.put("sureName",  uiew.getSurename().getText());
				return autoLogin(prefs.get("uname", ""), prefs.get("password", ""));
			} else if (tmp.equals("Database error/Username is occupied")) {
				JOptionPane.showMessageDialog(this, resources.getString("userNameOccupied"));
			} else {
				JOptionPane.showMessageDialog(this, resources.getString("unableToLogIn"));
			}
		} catch (UnknownHostException e) {
			Logging.printStackTrace(e);
			JOptionPane.showMessageDialog(this, resources.getString("unknownServerError"));
		} catch (IOException e) {
			Logging.printStackTrace(e);
			JOptionPane.showMessageDialog(this, resources.getString("serverCommunicationsError"));
		}
		return false;
	}

	private boolean checkNewUserDetails(UserInfoEntryWindow uiew) {
		if (uiew.getServerName().getText().length()<9) { 
			JOptionPane.showMessageDialog(Client.this, resources.getString("userInfoEntry.serverNameToShort"));
			return false;
		} else if (uiew.getNickname().getText().length()<6) {
			JOptionPane.showMessageDialog(Client.this, resources.getString("userInfoEntry.nicknameToShort"));
			return false;
		} else if (!uiew.getPassword().getText().equals(uiew.getVerifyPassword().getText())) {
			JOptionPane.showMessageDialog(Client.this, resources.getString("userInfoEntry.passwordsNotEqual"));
			return false;
		} else if (uiew.getPassword().getText().length()<6) {
			JOptionPane.showMessageDialog(Client.this, resources.getString("userInfoEntry.passwordToShort"));
			return false;
		}
		return true;
	}

	private JMenuBar createMenuBar() {
		JMenuBar menuBar = new JMenuBar ();
		JMenu system = new JMenu(resources.getString("mainMenu.system"));
		JMenuItem preferences = new JMenuItem(resources.getString("mainMenu.system.preferences"));
		system.add(preferences);
		preferences.addActionListener(e -> {
			JDialog enterUserPreferences = new UserPreferencesDialog(this);
		});
		JMenuItem connect = new JMenuItem(resources.getString("mainMenu.system.connect"));
		system.add(connect);
		connect.addActionListener (e -> {
			try {
				if (!prefs.get("serverName", "").equals("")) {
					if (autoLogin(prefs.get("uname", ""), prefs.get("password", ""))) {
						Component parts[] = getContentPane().getComponents();
						for (Component part: parts)
							remove(part);
						add (desktop);
						validate();
						openGlobalChat();
						startListener();
					}
				}
				else
					JOptionPane.showMessageDialog(this, resources.getString("warning.needUsernamePwdToConnect"));
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		menuBar.add (system);
		JMenuItem exit = new JMenuItem(resources.getString("mainMenu.system.exit"));
		system.add(exit);
		exit.addActionListener(e->System.exit(0));
		JMenu game = new JMenu (resources.getString("mainMenu.game"));
		JMenuItem joinGame = new JMenuItem (resources.getString("mainMenu.game.join"));
		joinGame.addActionListener(e -> joinGame());
		game.add(joinGame);
		menuBar.add(game);
		return menuBar;
	}

	private void startNewGame(String[] command) {
		Game g = new Game(command[2], Long.parseLong(command[1]), output);
		for (int i=0; i<4; i++)
			if (command.length>2+i*2) {
				g.setPlayerId (i, Integer.parseInt(command[3+i*2]));
				g.setPlayer (i, command[4+i*2]);
			}
		games.put(g.getId(), g);
		desktop.add(g);
		g.setVisible(true);
	}
	
	private void joinGame() {
		// Game game = new Game("Test");
		// desktop.add(game);
		// game.setVisible (true);
		output.println("JOIN RANDOM GAME");
		output.flush();
	}

	private void loginConnect() throws UnknownHostException {
		char[] keystorePass=sslPwd.toCharArray();

		//load own keystore 
		KeyStore keyStore;
		try {
			keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
			keyStore.load(getClass().getResourceAsStream(sslFName),keystorePass);

			//create a factory
			TrustManagerFactory     trustManagerFactory=TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init(keyStore);

			//get context
			SSLContext sslContext=SSLContext.getInstance("TLS");

			//init context
			sslContext.init(null, trustManagerFactory.getTrustManagers(), new SecureRandom());

			//create the socket
			connection=sslContext.getSocketFactory().createSocket(serverName, loginPortNumber);
			// connection.setKeepAlive(true);
			output = new PrintWriter (new OutputStreamWriter(connection.getOutputStream()));
			input = new BufferedReader (new InputStreamReader(connection.getInputStream()));
		} catch (KeyStoreException e1) {
			Logging.printStackTrace(e1);
		} catch (NoSuchAlgorithmException e) {
			Logging.printStackTrace(e);
		} catch (CertificateException e) {
			Logging.printStackTrace(e);
		} catch (KeyManagementException e) {
			Logging.printStackTrace(e);
		} catch (IOException e) {
			Logging.printStackTrace(e);
		}
	}
	
	public static void main(String[] args) {
		Client c;
		if (args.length==0)
			c = new Client(null, null);
		else
			c = new Client(args[0], MD5.MD5(args[1]));
		if (args.length>2) {
			for (int i=2; i<args.length; i++) {
				if (args[i].equals("--spammer")) {
					if (args.length>i) {
						int interval = Integer.parseInt(args[i+1]);
						Thread t = new ChatSpammer (c.globalChat, interval);
						t.setDaemon(true);
						t.start();
					}
				} else if (args[i].equals("--joinGame")) {
					c.joinGame();
				}
			}
		}
	}
}
