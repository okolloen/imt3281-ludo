package no.hig.imt3281.ludo.client;

import i18n.I18N;

import java.awt.BorderLayout;
import java.io.PrintWriter;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;
import javax.swing.border.BevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;

import no.hig.imt3281.ludo.Logging;

public class Chat extends JInternalFrame {
	private ResourceBundle resources = I18N.getClientBundle();
	private PrintWriter output;
	private int channelId;
	private String channelName;
	private JTextPane content = new JTextPane();
	private JTextField textToSend = new JTextField();
	private JScrollPane scrollPane;
	private JButton send = new JButton (resources.getString("chat.button.send"));
	
	public Chat (int id, String name, PrintWriter output) {
		super(name, true, true, true, true);
		this.output = output;
		channelId = id;
		channelName = name;
		JPanel contentPanel = new JPanel(new BorderLayout());
		contentPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		contentPanel.add(scrollPane = new JScrollPane(content));
		add (contentPanel, BorderLayout.CENTER);
		
		JPanel sendPanel = new JPanel (new BorderLayout());
		sendPanel.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
		sendPanel.add(textToSend, BorderLayout.CENTER);
		sendPanel.add(send, BorderLayout.EAST);
		add(sendPanel, BorderLayout.SOUTH);
		textToSend.addActionListener(e->send.doClick());
		send.addActionListener(e->{
			output.printf("MESSAGE\t%d\t%s\n", channelId, textToSend.getText());
			output.flush();
			textToSend.setText("");
			textToSend.requestFocus();
		});
	}
	
	public JTextField getTextField() {
		return textToSend;
	}
	public void addMessage (String nickname, String message, boolean myMessage) {
		SimpleAttributeSet sas = new SimpleAttributeSet ();
		//StyleConstants.setFontFamily (sas, (String) fontName.getSelectedItem());
		StyleConstants.setFontSize (sas, 12);
		//StyleConstants.setBold(sas, bold.isSelected());
		if (myMessage)
			StyleConstants.setItalic(sas, true);
		//StyleConstants.setForeground(sas, foreColor.getBackground());
		// We want to add the text at the end of the existing text
		int pos = content.getStyledDocument().getEndPosition().getOffset();
	    try {
		    	// add the text to the document
		    	content.getStyledDocument().insertString(pos, nickname+": ", sas);
		    	StyleConstants.setItalic(sas, false);
		    	pos = content.getStyledDocument().getEndPosition().getOffset();
		    	content.getStyledDocument().insertString(pos, message+"\n", sas);
	    } catch (BadLocationException ble) {
	    		Logging.printStackTrace(ble);
	    }
	    SwingUtilities.invokeLater(()->{
	    		scrollPane.getVerticalScrollBar().setValue(scrollPane.getVerticalScrollBar().getMaximum());
	    });
	}

	public JButton getSendButton() {
		return send;
	}
}
