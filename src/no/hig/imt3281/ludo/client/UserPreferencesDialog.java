package no.hig.imt3281.ludo.client;

import i18n.I18N;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import no.hig.imt3281.ludo.util.MD5;

public class UserPreferencesDialog extends JDialog {
	private final static ResourceBundle resources = I18N.getClientBundle();
	private static Preferences prefs;
	private JTextField server, uname;
	private JPasswordField pwd;
	private JButton ok, cancel;
	
	public UserPreferencesDialog(Frame owner) {
		super(owner, resources.getString("userPrefencesDialog.window.title"), true);
		prefs = Preferences.userNodeForPackage(getClass());
		GridBagLayout gbl = new GridBagLayout();
		setLayout (gbl);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.anchor = GridBagConstraints.EAST;
		addComponent (new JLabel (resources.getString("userPreferencesDialog.label.serverName")), gbl, gbc, 1, 1);
		addComponent (new JLabel (resources.getString("userPreferencesDialog.label.uname")), gbl, gbc, 1, 2);
		addComponent (new JLabel (resources.getString("userPreferencesDialog.label.password")), gbl, gbc, 1, 3);
		addComponent (ok = new JButton(resources.getString("okButton")), gbl, gbc, 1, 4);
		ok.addActionListener(e->{
			prefs.put("uname", uname.getText());
			prefs.put("serverName", server.getText());
			prefs.put("password", MD5.MD5(pwd.getText()));
			setVisible(false);
			dispose();
		});
		
		gbc.anchor = GridBagConstraints.WEST;
		addComponent (server = new JTextField(20), gbl, gbc, 2, 1);
		addComponent (uname = new JTextField(20), gbl, gbc, 2, 2);
		addComponent (pwd = new JPasswordField(20), gbl, gbc, 2, 3);
		addComponent (cancel = new JButton(resources.getString("cancelButton")), gbl, gbc, 2, 4);
		cancel.addActionListener(e->{
			setVisible(false);
			dispose();
		});
		pack();
		setVisible(true);
	}

	private void addComponent(JComponent c, GridBagLayout gbl, GridBagConstraints gbc, int x, int y) {
		gbc.gridx = x;
		gbc.gridy = y;
		gbl.setConstraints(c, gbc);
		add (c);		
	}
}
