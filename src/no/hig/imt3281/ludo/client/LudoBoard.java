package no.hig.imt3281.ludo.client;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import no.hig.imt3281.ludo.Logging;
import no.hig.imt3281.ludo.client.LudoBoard.LudoLogic;

public class LudoBoard extends JPanel {
	private Dimension size;
	private Image board;
	private Vector<Point> squares = new Vector<>();
	private final LudoLogic logic = new LudoLogic();
	public static final int RED = 0;
	public static final int BLUE = 1;
	public static final int YELLOW = 2;
	public static final int GREEN = 3;
	public int lastClickedSquare = -1; 
	public int activePlayer = -1;
	private int placesToMove;
	private ActionListener pieceSelected = null;
	private ActionListener confirmMove = null;
	private int selectedPiece = -1;
	private int diceValue;
	
	public LudoBoard () {
		ImageIcon tmpBoard = new ImageIcon (getClass().getResource("/resources/ludoBoard.png"	));
		int height = tmpBoard.getIconHeight();
		int width = tmpBoard.getIconWidth();
		size = new Dimension(width, height);
		board = tmpBoard.getImage();
		defineSquares();
		addMouseListener(new MouseAdapter () {
			public void mouseClicked (MouseEvent me) {
				if (diceValue<1||diceValue>6)
					return;
				lastClickedSquare = mouseClickedOnSquare(me);
				if (selectedPiece>-1) {
					int startPos = logic.getPosition(activePlayer, selectedPiece);
					int endPos=-1;
					if (startPos==-1&&diceValue==6)
						endPos=4;
					else if (startPos>-1)
						endPos=startPos+diceValue;
					if (logic.playerPosToBoardPos(activePlayer, endPos)==lastClickedSquare&&logic.canMove(activePlayer, startPos, endPos)&&confirmMove!=null) {
						Logging.getLogger().info("Player: "+activePlayer+"\tstart:"+startPos+"\tend:"+endPos);
						confirmMove.actionPerformed(new ActionEvent(this, endPos, "MOVE TO"));
						return;
					}
				}
				selectedPiece = -1;
				if (lastClickedSquare>-1&&pieceSelected!=null) {
					Logging.getLogger().info("User clicked "+lastClickedSquare+" : Player "+activePlayer);
					for (int piece=0; piece<4; piece++) {
						if (logic.getPosition(activePlayer, piece)>-1) {
							if (logic.playerPosToBoardPos(activePlayer, logic.getPosition(activePlayer, piece))==lastClickedSquare)
								selectedPiece = piece;
						} else {
							if (logic.playerPosToBoardPos(activePlayer,piece)==lastClickedSquare&&diceValue==6)
								selectedPiece = piece;
						}
					}
				}
				pieceSelected.actionPerformed(new ActionEvent(this, selectedPiece, "PIECE SELECTED"));
				repaint();
			}
		});
	}
	
	public void setDiceValue (int dice) {
		diceValue = dice;
	}
	
	public void setPieceSelectedListener (ActionListener pieceSelected) {
		this.pieceSelected = pieceSelected;
	}
	
	public void setConfirmMoveListener (ActionListener confirmMove) {
		this.confirmMove = confirmMove;
	}
	
	void drawPieces(Graphics2D g2d) {
		Color colors[] = new Color[4];
		colors[RED] = Color.RED;
		colors[BLUE] = Color.BLUE;
		colors[GREEN] = Color.GREEN;
		colors[YELLOW] = Color.YELLOW;
		for (int color=0; color<4; color++) {
			g2d.setStroke(new BasicStroke(2f));
			for (int piece=0; piece<4; piece++) {
				if (logic.getPosition(color, piece)==-1) {
					Point p = squares.get(logic.playerPosToBoardPos(color, piece));
					g2d.setPaint(Color.DARK_GRAY);
					g2d.fillOval(p.x+9, p.y+9, 33, 33);
					g2d.setPaint(colors[color]);
					g2d.fillOval(p.x+10, p.y+10, 30, 30);
				} else {
					Point p = squares.get(logic.playerPosToBoardPos(color, logic.getPosition(color, piece)));
					g2d.setPaint(Color.DARK_GRAY);
					g2d.fillOval(p.x+9, p.y+9, 33, 33);
					g2d.setPaint(colors[color]);
					g2d.fillOval(p.x+10, p.y+10, 30, 30);
				}
			}
		}
	}

	private int mouseClickedOnSquare(MouseEvent me) {
		int x = me.getX();
		int y = me.getY();
		for (Point p : squares) {
			if (p.x<x&&p.x>x-52&&p.y<y&&p.y>y-52)
				return squares.indexOf(p);
		};
		return -1;
	}

	/**
	 * Set uper left corner for all places on the board where a piece can be places.
	 */
	private void defineSquares() {
		squares.add(new Point(427, 55));						// Red start
		squares.add(new Point(427, 55+53));
		squares.add(new Point(427, 55+53*2));
		squares.add(new Point(427, 55+53*3));
		squares.add(new Point(427, 55+53*4));
		squares.add(new Point(427+54, 56+53*5));
		squares.add(new Point(427+54*2, 56+53*5));
		squares.add(new Point(426+54*3, 56+53*5));
		squares.add(new Point(425+54*4, 56+53*5));
		squares.add(new Point(424+54*5, 56+53*5));
		squares.add(new Point(423+54*6, 56+53*5));
		squares.add(new Point(423+54*6, 56+53*6));
		squares.add(new Point(423+54*6, 56+53*7));
		squares.add(new Point(424+54*5, 56+53*7));	// Blue start
		squares.add(new Point(425+54*4, 56+53*7));
		squares.add(new Point(426+54*3, 56+53*7));
		squares.add(new Point(427+54*2, 56+53*7));
		squares.add(new Point(427+54, 56+53*7));
		squares.add(new Point(427, 57+53*8));
		squares.add(new Point(427, 57+53*9));
		squares.add(new Point(427, 57+53*10));
		squares.add(new Point(427, 58+53*11));
		squares.add(new Point(427, 58+53*12));
		squares.add(new Point(427, 58+53*13));
		squares.add(new Point(427-53, 58+53*13));
		squares.add(new Point(427-53*2, 58+53*13));
		squares.add(new Point(427-53*2, 58+53*12));	// Yellow start
		squares.add(new Point(427-53*2, 58+53*11));
		squares.add(new Point(427-53*2, 57+53*10));
		squares.add(new Point(427-53*2, 57+53*9));
		squares.add(new Point(427-53*2, 57+53*8));
		squares.add(new Point(427-53*3, 56+53*7));
		squares.add(new Point(427-53*4, 56+53*7));
		squares.add(new Point(427-53*5, 56+53*7));
		squares.add(new Point(427-53*6, 56+53*7));
		squares.add(new Point(427-53*7, 56+53*7));
		squares.add(new Point(427-53*8, 56+53*7));
		squares.add(new Point(427-53*8, 56+53*6));
		squares.add(new Point(427-53*8, 56+53*5));
		squares.add(new Point(427-53*7, 56+53*5));		// Green start
		squares.add(new Point(427-53*6, 56+53*5));
		squares.add(new Point(427-53*5, 56+53*5));
		squares.add(new Point(427-53*4, 56+53*5));
		squares.add(new Point(427-53*3, 56+53*5));
		squares.add(new Point(427-53*2, 55+53*4));
		squares.add(new Point(427-53*2, 55+53*3));
		squares.add(new Point(427-53*2, 55+53*2));
		squares.add(new Point(427-53*2, 55+53));
		squares.add(new Point(427-53*2, 55));
		squares.add(new Point(427-53*2, 55-53));
		squares.add(new Point(427-53, 55-53));
		squares.add(new Point(427, 55-53));
		// Red homestretch
		squares.add(new Point(427-53, 55));
		squares.add(new Point(427-53, 55+53));
		squares.add(new Point(427-53, 55+53*2));
		squares.add(new Point(427-53, 55+53*3));
		squares.add(new Point(427-53, 55+53*4));
		squares.add(new Point(427-53, 58+53*5));
		// Blue homestretch
		squares.add(new Point(424+54*5, 56+53*6));
		squares.add(new Point(425+54*4, 56+53*6));
		squares.add(new Point(426+54*3, 56+53*6));
		squares.add(new Point(427+54*2, 56+53*6));
		squares.add(new Point(427+54, 56+53*6));
		squares.add(new Point(425, 56+53*6));
		// Yellow homestretch
		squares.add(new Point(427-53, 58+53*12));
		squares.add(new Point(427-53, 58+53*11));
		squares.add(new Point(427-53, 57+53*10));
		squares.add(new Point(427-53, 57+53*9));
		squares.add(new Point(427-53, 57+53*8));
		squares.add(new Point(427-53, 55+53*7));
		// Green homestretch
		squares.add(new Point(427-53*7, 56+53*6));	
		squares.add(new Point(427-53*6, 56+53*6));
		squares.add(new Point(427-53*5, 56+53*6));
		squares.add(new Point(427-53*4, 56+53*6));
		squares.add(new Point(427-53*3, 56+53*6));
		squares.add(new Point(429-53*2, 56+53*6));
		// Red prestart
		squares.add(new Point(613, 82));
		squares.add(new Point(613-53, 82+53));
		squares.add(new Point(613+53, 82+53));
		squares.add(new Point(613, 82+53*2));
		// Blue prestart
		squares.add(new Point(613, 479+82));
		squares.add(new Point(613-53, 479+82+53));
		squares.add(new Point(613+53, 479+82+53));
		squares.add(new Point(613, 479+82+53*2));
		// Yellow prestart
		squares.add(new Point(613-479, 479+82));
		squares.add(new Point(613-53-479, 479+82+53));
		squares.add(new Point(613+53-479, 479+82+53));
		squares.add(new Point(613-479, 479+82+53*2));
		// Green prestart
		squares.add(new Point(613-479, 82));
		squares.add(new Point(613-53-479, 82+53));
		squares.add(new Point(613+53-479, 82+53));
		squares.add(new Point(613-479, 82+53*2));
	}
	
	@Override
	public Dimension getPreferredSize() {
		return size;
	}
	
	@Override
	public Dimension getMinimumSize() {
		return size;
	}
	
	public void moveSelectedPieceTo(int id) {
		logic.movePiece(activePlayer, logic.getPosition(activePlayer, selectedPiece), id);
		repaint();
	}
	
	public void movePiece(int player, int piece, int to) {
		logic.movePiece(player, logic.getPosition(player, piece), to);
		repaint();
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Graphics2D g2d = (Graphics2D)g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
		        RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.drawImage(board, 0, 0, this);
/*		if (lastClickedSquare>-1&&placesToMove>0) {
			g2d.setPaint(new Color(.3f,.3f,.3f));
			g2d.fillRect(squares.get(lastClickedSquare).x, squares.get(lastClickedSquare).y, 51, 51);
		} */
		if (selectedPiece>-1) {
			g2d.setPaint(new Color(.3f,.3f,.3f,.5f));
			int position = logic.getPosition(activePlayer, selectedPiece);
			int endPosition;
			if (position==-1) {
				position = selectedPiece;
				endPosition = 4;
			} else
				endPosition = position+diceValue;
			g2d.fillRect(squares.get(logic.playerPosToBoardPos(activePlayer, position)).x, squares.get(logic.playerPosToBoardPos(activePlayer, position)).y, 51, 51);			
			if (logic.canMove(activePlayer, logic.getPosition(activePlayer, selectedPiece), endPosition)) {
				g2d.setPaint(new Color(.3f,.9f,.3f,.5f));
				g2d.fillRect(squares.get(logic.playerPosToBoardPos(activePlayer, endPosition)).x, squares.get(logic.playerPosToBoardPos(activePlayer, endPosition)).y, 51, 51);					
			}
		}
		drawPieces(g2d);
//		showSquares(g2d);	// Visualize the squares and places arrays
	}

	public static void main(String[] args) {
		JFrame f = new JFrame("Demo");
		LudoBoard lb = new LudoBoard();
		f.add(lb);
		f.pack();
		f.setVisible(true);
	}
	
	private void showSquares(Graphics2D g2d) {
		g2d.setPaint(new Color(.3f,.3f,.3f));
		g2d.setStroke(new BasicStroke(2));
		squares.stream().forEach(point -> {
			g2d.fillRect(point.x, point.y, 51, 51);
		});
		g2d.setPaint(Color.WHITE);
		g2d.setFont(new Font("Serif", Font.PLAIN , 15));
		for (int i=0; i<squares.size(); i++) {
			g2d.drawString(i+"", squares.get(i).x+15, squares.get(i).y+30);
		}
		g2d.setPaint(new Color(1f, 0.3f, 0.3f));
		for (int i=0; i<logic.places[0].length; i++) {
			g2d.drawString(i+"", squares.get(logic.places[LudoLogic.RED][i]).x+5, squares.get(logic.places[LudoLogic.RED][i]).y+15);
		}
		g2d.setPaint(new Color(0.3f, 0.6f, 1f));
		for (int i=0; i<logic.places[0].length; i++) {
			g2d.drawString(i+"", squares.get(logic.places[LudoLogic.BLUE][i]).x+30, squares.get(logic.places[LudoLogic.BLUE][i]).y+15);
		}
		g2d.setPaint(Color.YELLOW);
		for (int i=0; i<logic.places[0].length; i++) {
			g2d.drawString(i+"", squares.get(logic.places[LudoLogic.YELLOW][i]).x+5, squares.get(logic.places[LudoLogic.YELLOW][i]).y+45);
		}
		g2d.setPaint(Color.GREEN);
		for (int i=0; i<logic.places[0].length; i++) {
			g2d.drawString(i+"", squares.get(logic.places[LudoLogic.GREEN][i]).x+30, squares.get(logic.places[LudoLogic.GREEN][i]).y+45);
		}
	}

	public static class LudoLogic {
		private int[][] places = new int[4][63];
		public static final int RED = 0;
		public static final int BLUE = 1;
		public static final int YELLOW = 2;
		public static final int GREEN = 3;
		private int piecePosition[][] = new int[4][4];

		public LudoLogic () {
			definePlaces();
			for (int i=0; i<4; i++) {
				piecePosition[RED][i] = -1;
				piecePosition[BLUE][i] = -1;
				piecePosition[YELLOW][i] = -1;
				piecePosition[GREEN][i] = -1;
			}
		}
		
		int playerPosToBoardPos(int color, int pos) {
			return places[color][pos];
		}
		
		public boolean canMove(int color, int from, int to) {
			// System.out.printf ("canMoveTo: %d:%d [%d, %d, %d, %d]\n", from, to, piecePosition[color][0], piecePosition[color][1], piecePosition[color][2], piecePosition[color][3]);
			// Can a player move from starting grid and out onto the playing field
			if (from==-1&&to==4&&((piecePosition[color][0] == -1)||(piecePosition[color][1] == -1)||
					(piecePosition[color][2] == -1)||(piecePosition[color][2] == -1))&&opponentsCount(color, to)<2)
				return true;
			// Can not move from starting grid and anywhere BUT the first field
			if (from==-1&&to!=4)
				return false;
			// Can not move past goal
			if (to>62)
				return false;
			// Can move up to six fields if not blocked 
			if (from>3&&to>from&&to-from<7) {
				for (int i=from; i<=to; i++)
					if (opponentsCount(color, i)>1)
						return false;
				return true;
			}
			return false;
		}
		
		public boolean canUse(int color, int diece) {
			if (diece==6)
				if (canMove(color, -1, 4))
					return true;
			for (int piece=0; piece<4; piece++) {
				if (piecePosition[color][piece]==-1)	// Piece in starting position
					continue;
				if (piecePosition[color][piece]+diece>62)	// Would move past goald
					continue;
				if (canMove(color, piecePosition[color][piece], piecePosition[color][piece]+diece))
					return true;
			}
			return false;
		}
		
		public boolean movePiece(int color, int from, int to) {
			Logging.getLogger().info("Moving piece from "+from+" to "+to+" for player "+color);
			for (int piece=0; piece<4; piece++) {
				if (piecePosition[color][piece]==from) {
					piecePosition[color][piece] = to;
					return true;
				}
			}
			return false;
		}
		
		int opponentsCount(int color, int pos) {
			int countForPos = playerPosToBoardPos(color, pos);
			// System.out.printf ("opponentsCount: %d = %d, %d\n", countForPos, color, pos);
			for (int i=0; i<4; i++) {				// Loop through all colors
				if (i==color) continue;			// This is the players own color
				int count=0;							
				for (int ii=0; ii<4; ii++) {		// Count number of this players pieces on this position
					// System.out.printf ("opponentsCount: %d, %d: %d == %d\n", i, ii, piecePosition[i][ii], countForPos);
					if (piecePosition[i][ii]>-1&& playerPosToBoardPos(i, piecePosition[i][ii]) == countForPos) {
						count++;
					}
				}
				if (count>0)							// Only one player can have pieces on any given positon
					return count;					// so if piece(s) found, return that number
			}
			// System.out.printf("opponentsCount returning 0\n");
			return 0;									// No pieces found
		}

		int getPosition(int color, int piece) {
			return piecePosition[color][piece];
		}
		
		/**
		 * Creates a map between the position of player pieces and the location array for the entire board.
		 */
		private void definePlaces() {
			// Red places
			places[RED][0] = 76;
			places[RED][1] = 77;
			places[RED][2] = 78;
			places[RED][3] = 79;
			for (int i=0; i<52; i++)
				places[RED][4+i] = i;
			places[RED][56] = 0;
			for (int i=0; i<6; i++)
				places[RED][57+i] = 52+i;
			// Blue places
			places[BLUE][0] = 80;
			places[BLUE][1] = 81;
			places[BLUE][2] = 82;
			places[BLUE][3] = 83;
			for (int i=0; i<52; i++)
				places[BLUE][4+i] = (i+13)%52;
			places[BLUE][56] = 13;
			for (int i=0; i<6; i++)
				places[BLUE][57+i] = 58+i;
			// Yellow places
			places[YELLOW][0] = 84;
			places[YELLOW][1] = 85;
			places[YELLOW][2] = 86;
			places[YELLOW][3] = 87;
			for (int i=0; i<52; i++)
				places[YELLOW][4+i] = (i+26)%52;
			places[YELLOW][56] = 26;
			for (int i=0; i<6; i++)
				places[YELLOW][57+i] = 64+i;
			// Green places
			places[GREEN][0] = 88;
			places[GREEN][1] = 89;
			places[GREEN][2] = 90;
			places[GREEN][3] = 91;
			for (int i=0; i<52; i++)
				places[GREEN][4+i] = (i+39)%52;
			places[GREEN][56] = 39;
			for (int i=0; i<6; i++)
				places[GREEN][57+i] = 70+i;
		}
	}

	public LudoLogic getLudoLogic() {
		return logic;
	}

	public void activePlayer(int player) {
		activePlayer = player;
	}

	public void setSelectedPiece(int id) {
		selectedPiece = id;
	}

	public int getSelectedPiece() {
		return selectedPiece;
	}
}
