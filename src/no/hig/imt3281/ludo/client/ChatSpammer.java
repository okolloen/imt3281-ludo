package no.hig.imt3281.ludo.client;

public class ChatSpammer extends Thread {
	Chat room;
	int interval;
	String texts[] = {"Doing fine thank you I am.", "How today are you, hmm?  Yes, hmmm.", 
							"With you may the force be.  Hmmmmmm.", "You, go away will!",
							"Your hands up put, and step away from the turnip.  Yeesssssss.",
							"This irresistible grasp can I escape, hmm?  Yes, hmmm.",
							"Forming on the tips of my wings, ice is.",
							"You must unlearn what you have learned.",
							"Size matters not. Look at me. Judge me by my size, do you? Hmm? Hmm.",
							"Help you I can. Yes, mmmm. ",
							"Control, control, you must learn control!"};
	
	public ChatSpammer(Chat room, int interval) {
		this.room = room;
		this.interval = interval;
	}

	public void run() {
		while (true) {
			try {
				sleep (interval);
				if (room!=null) {
					room.getTextField().setText(texts[(int)(Math.random()*texts.length)]);
					room.getSendButton().doClick();
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
}
