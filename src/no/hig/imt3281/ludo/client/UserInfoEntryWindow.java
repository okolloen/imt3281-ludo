package no.hig.imt3281.ludo.client;

import i18n.I18N;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ResourceBundle;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

public class UserInfoEntryWindow extends JInternalFrame {
	private JTextField nickname, givenname, surename, email, serverName;
	private JPasswordField password, verifyPassword;
	private JButton connect;
	private static ResourceBundle resources = I18N.getClientBundle();
	
	public UserInfoEntryWindow () {
		super (resources.getString("userInfoEntryWindow.windowTitle"));
		GridBagLayout gbl = new GridBagLayout();
		setLayout (gbl);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(2, 2, 2, 2);
		gbc.anchor = GridBagConstraints.EAST;
		addComponent (new JLabel (resources.getString("userInfoEntryWindow.label.serverName")), gbl, gbc, 1, 1);
		addComponent (new JLabel (resources.getString("userInfoEntryWindow.label.email")), gbl, gbc, 1, 2);
		addComponent (new JLabel (resources.getString("userInfoEntryWindow.label.nickname")), gbl, gbc, 1, 3);
		addComponent (new JLabel (resources.getString("userInfoEntryWindow.label.password")), gbl, gbc, 1, 4);
		addComponent (new JLabel (resources.getString("userInfoEntryWindow.label.verifyPassword")), gbl, gbc, 1, 5);
		addComponent (new JLabel (resources.getString("userInfoEntryWindow.label.givenname")), gbl, gbc, 1, 6);
		addComponent (new JLabel (resources.getString("userInfoEntryWindow.label.surename")), gbl, gbc, 1, 7);
		gbc.anchor = GridBagConstraints.WEST;
		addComponent (serverName = new JTextField(20), gbl, gbc, 2, 1);
		addComponent (email = new JTextField(20), gbl, gbc, 2, 2);
		addComponent (nickname = new JTextField(20), gbl, gbc, 2, 3);
		addComponent (password = new JPasswordField(20), gbl, gbc, 2, 4);
		addComponent (verifyPassword = new JPasswordField(20), gbl, gbc, 2, 5);
		addComponent (givenname = new JTextField(20), gbl, gbc, 2, 6);
		addComponent (surename = new JTextField(20), gbl, gbc, 2, 7);
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridwidth = 2;
		addComponent(connect = new JButton(resources.getString("userInfoEntryWindow.button.connect")), gbl, gbc, 1, 8);
	}

	public JTextField getNickname() {
		return nickname;
	}

	public JTextField getGivenname() {
		return givenname;
	}

	public JTextField getSurename() {
		return surename;
	}

	public JTextField getEmail() {
		return email;
	}

	public JTextField getServerName() {
		return serverName;
	}

	public JPasswordField getPassword() {
		return password;
	}

	public JPasswordField getVerifyPassword() {
		return verifyPassword;
	}

	public JButton getConnect() {
		return connect;
	}

	private void addComponent(JComponent c, GridBagLayout gbl, GridBagConstraints gbc, int x, int y) {
		gbc.gridx = x;
		gbc.gridy = y;
		gbl.setConstraints(c, gbc);
		add (c);		
	}
}
