package no.hig.imt3281.ludo.client;

import i18n.I18N;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.io.PrintWriter;
import java.util.Random;
import java.util.ResourceBundle;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import no.hig.imt3281.ludo.Logging;

public class Game extends JInternalFrame {
	private final static ResourceBundle resources = I18N.getClientBundle();
	private JLabel player1, player2, player3, player4 ;
	private int playerId[] = new int[4];
	private JTextPane chat = new JTextPane();
	private JTextField toSay = new JTextField (); 
	private JScrollPane chatScrollPane;
	private JButton throwDice;
	private Dice dice = new Dice();
	private int playerPosition[][] = new int[4][4];
	private boolean diceIsRolling = false;
	private int diceRolledTimes = 0;
	private long id;
	private LudoBoard board = new LudoBoard();
	private int player;			// What color is this player
	private PrintWriter output;
	private boolean myTurn = false;

	public Game (String title, long id, PrintWriter output) {
		super(title, true, true, true, true);
		this.id = id;
		this.output = output;
		setLayout (new BorderLayout());
		add (board);
		board.setPieceSelectedListener(e->pieceSelected(e));
		board.setConfirmMoveListener(e->confirmMove(e));
		JPanel players = playerGrid();
		JPanel right = new JPanel(new BorderLayout());
		right.add(players, BorderLayout.NORTH);
		add(right, BorderLayout.EAST);
		for (int i=0; i<4; i++)
			for (int ii=0; ii<4; ii++)
				playerPosition[i][ii] = -1;
		JPanel lowerRight = new JPanel (new BorderLayout());
		lowerRight.add(throwDicePanel(), BorderLayout.NORTH);
		right.add (lowerRight, BorderLayout.CENTER);
		JPanel chatPanel = new JPanel(new BorderLayout());
		chatPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), resources.getString("game.frametitle.chat")));
		chatPanel.add(chatScrollPane = new JScrollPane(chat), BorderLayout.CENTER);
		chatPanel.add(toSay, BorderLayout.SOUTH);
		toSay.setToolTipText(resources.getString("game.textField.toSay.tooltip"));
		lowerRight.add(chatPanel, BorderLayout.CENTER);
		pack ();
		Roller rollDice = new Roller();
		rollDice.setDaemon(true);
		rollDice.start();
	}

	private void confirmMove(ActionEvent e) {
		if (!myTurn)
			return;
		output.printf("MOVE\t%d\t%d\t%d\t%d\n", id, player, board.getSelectedPiece(), e.getID());
		output.flush();
		board.moveSelectedPieceTo(e.getID());
		board.setSelectedPiece(-1);
		Logging.getLogger().info("Move selected piece to: "+e.getID());
		// *******************************************************************
		// CHANGE THIS TO REFLECT PLAYERS CURRENT STATUS
		/* TODO: Change this
		 * 
		 */
		dice.resetDice();
		board.setDiceValue(10);	// Should never move more than 6, this blocks until the dice has been thrown again
		myTurn = false;
		output.printf("PASS\t%d\t%d\n", id, player);
		output.flush();
	}

	private void pieceSelected(ActionEvent e) {
		if (!myTurn)
			return;
		board.setSelectedPiece (e.getID());
		Logging.getLogger().info("Selected piece:"+e.getID());
	}

	private JPanel throwDicePanel() {
		JPanel throwDicePanel = new JPanel(new BorderLayout());
		throwDicePanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), resources.getString("game.frametitle.throwDice")));
		throwDicePanel.add(throwDice = new JButton(resources.getString("game.button.throwDice")), BorderLayout.WEST);
		throwDice.setToolTipText(resources.getString("game.button.throwDice.tooltip"));
		throwDice.addActionListener(e->rollTheDice());
		throwDice.setEnabled(false);
		JPanel dicePanel = new JPanel();
		dicePanel.setBorder(BorderFactory.createLoweredBevelBorder());
		dicePanel.add (dice);
		throwDicePanel.add(dicePanel);
		return throwDicePanel;
	}

	public void rollTheDice() {
		diceIsRolling = true;
		diceRolledTimes = 0;
	}
	
	public void stopTheDice() {
		diceIsRolling = false;
	}
	
	private JPanel playerGrid() {
		JPanel players = new JPanel (new GridLayout(2,2,5,5));
		players.setPreferredSize(new Dimension(250,150));
		players.setBorder(BorderFactory.createTitledBorder(BorderFactory.createRaisedBevelBorder(), resources.getString("game.frametitle.players")));
		players.add(player4 = new JLabel("  Player 4"));
		players.add(player1 = new JLabel("  Player 1"));
		players.add(player3 = new JLabel("  Player 3"));
		players.add(player2 = new JLabel("  Player 2"));
		player1.setOpaque(true);
		player1.setBackground(Color.RED);
		player1.setForeground(Color.WHITE);
		player1.setBorder(BorderFactory.createLineBorder(Color.RED, 5));
		player1.setHorizontalTextPosition(SwingConstants.CENTER);
		player1.setVerticalTextPosition(SwingConstants.CENTER);
		player2.setOpaque(true);
		player2.setBackground(Color.BLUE);
		player2.setForeground(Color.WHITE);
		player2.setBorder(BorderFactory.createLineBorder(Color.BLUE, 5));
		player2.setHorizontalTextPosition(SwingConstants.CENTER);
		player2.setVerticalTextPosition(SwingConstants.CENTER);
		player3.setOpaque(true);
		player3.setBackground(Color.YELLOW);
		player3.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 5));
		player3.setHorizontalTextPosition(SwingConstants.CENTER);
		player3.setVerticalTextPosition(SwingConstants.CENTER);
		player4.setOpaque(true);
		player4.setBackground(Color.GREEN);
		player4.setBorder(BorderFactory.createLineBorder(Color.GREEN, 5));
		player4.setHorizontalTextPosition(SwingConstants.CENTER);
		player4.setVerticalTextPosition(SwingConstants.CENTER);
		return players;
	}
	
	class Roller extends Thread {
		public void run() {
			while (true) {
				try {
					sleep(50);
					if (diceIsRolling) {
						double v = Math.random()*6;
						int val = ((int)v)+1;
						//*************************************************************
						// FOR TESTING PURPOSE ONLY!!!!!!
						// NEED TO REMOVE THE NEXT LINE ON PRODUCTION
						//*
						val = 6;
						// REMOVE PREVIOUS LINE BEFORE PRODUCTION, WILL ALWAYS THROW A SIX
						dice.setDice(val);
						dice.repaint();
						diceRolledTimes++;
						if (diceRolledTimes>10) {
							diceIsRolling=false;
							userRolledDice(val);
						}
					}
				} catch (InterruptedException e) {
				}
			}
		}

		private void userRolledDice(int val) {
			throwDice.setEnabled(false);
			if (!board.getLudoLogic().canUse(player, val)) {
				diceRolledTimes = 0;
				myTurn = false;
				JOptionPane.showMessageDialog(Game.this, resources.getString("game.rolledDice.canNotUse"));
				output.printf("PASS\t%d\t%d\n", id, player);
				output.flush();
			} else {
				board.setDiceValue(val);
			}
		}
	}
	
	class Dice extends JPanel {
		Image dice[] = new Image[6];
		Dimension size;
		Image currentDice = null;
		
		public Dice () {
			ImageIcon tmp = new ImageIcon(getClass().getResource("/resources/dice1.png"));
			size = new Dimension (tmp.getIconWidth(), tmp.getIconHeight());
			dice[0] = tmp.getImage();
			dice[1] = new ImageIcon(getClass().getResource("/resources/dice2.png")).getImage();
			dice[2] = new ImageIcon(getClass().getResource("/resources/dice3.png")).getImage();
			dice[3] = new ImageIcon(getClass().getResource("/resources/dice4.png")).getImage();
			dice[4] = new ImageIcon(getClass().getResource("/resources/dice5.png")).getImage();
			dice[5] = new ImageIcon(getClass().getResource("/resources/dice6.png")).getImage();
		}
		
		public void setDice(int eyes) {
			if (eyes>0&&eyes<7)
				currentDice = dice[eyes-1];
		}
		
		public void resetDice() {
			currentDice = null;
		}
		
		public void paint(Graphics g) {
			super.paint(g);
			if (currentDice==null)
				return;
			Graphics2D g2d = (Graphics2D)g;
			g2d.drawImage(currentDice, 0, 0, size.width, size.height, null);
		}
		
		@Override
		public Dimension getPreferredSize() {
			return size;
		}
		
		@Override
		public Dimension getMinimumSize() {
			return size;
		}
	}

	public void setPlayer(int i, String string) {
		switch (i) {
			case 0: player1.setText("  "+string);
						break;
			case 1: player2.setText("  "+string);
						break;
			case 2: player3.setText("  "+string);
						break;
			case 3: player4.setText("  "+string);
						break;
		}
	}

	public Long getId() {
		return id;
	}

	public void setPlayerId(int i, int playerId) {
		this.playerId[i] = playerId;		
	}

	public void showActivePlayer(int player) {
		player1.setBorder(BorderFactory.createLineBorder(Color.RED, 5));
		player2.setBorder(BorderFactory.createLineBorder(Color.BLUE, 5));
		player3.setBorder(BorderFactory.createLineBorder(Color.YELLOW, 5));
		player4.setBorder(BorderFactory.createLineBorder(Color.GREEN, 5));

		switch (player) {
		case 0: player1.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 5));
			break;
		case 1: player2.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 5));
			break;
		case 2: player3.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 5));
			break;
		case 3: player4.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 5));
			break;
		}
		board.activePlayer(player);
	}

	public void myTurn(int i) {
		player = i;
		throwDice.setEnabled(true);
		myTurn = true;
	}

	public void movePiece(int player, int piece, int to) {
		board.movePiece(player, piece, to);
	}
}
