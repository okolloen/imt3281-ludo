package no.hig.imt3281.ludo.client;

import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class RemoveClient {

	public RemoveClient () {
		Preferences prefs = Preferences.userNodeForPackage(getClass());
		try {
			prefs.clear();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		new RemoveClient();

	}

}
