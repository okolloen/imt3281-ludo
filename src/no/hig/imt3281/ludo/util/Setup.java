package no.hig.imt3281.ludo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import no.hig.imt3281.ludo.Logging;

public class Setup {
	private final static String url = "jdbc:derby:ludoServer;create=true";
	private static Logger logger = Logging.getLogger();
	
	public static void setup () {
		try {
			logger.info("Creating database");
			Connection con =  DriverManager.getConnection(url);
			logger.info("Creating tables");
			Statement stmt = con.createStatement();
			// use execute to perform CREATE, INSERT, UPDATE, DELETE, executeQuery is for SELECT
			// MySQL has auto_increment, Derby has GENERATED values
			stmt.execute("CREATE TABLE users (id bigint NOT NULL GENERATED ALWAYS AS IDENTITY (START WITH 1, INCREMENT BY 1), "+
					"givenname varchar(128) NOT NULL, "+
					"surename varchar(128) NOT NULL, "+
					"nickname varchar(32) NOT NULL,"+
					"logins int,"+
					"lastLogin timestamp,"+
					"email varchar(128) NOT NULL, "+
					"password char(128) NOT NULL, "+
					"loginkey char(128), "+
					"loginhost char(128),"+
					"PRIMARY KEY  (id),"+
					"UNIQUE (email))");
			con.close();
			logger.info("Setup completed");
			JOptionPane.showMessageDialog(null, "Setup completed, you can now start the server");
		} catch (SQLException e) {
			Logging.printStackTrace(e);
		}
	}
	
	public static void main(String[] args) {
		setup ();
	}
}