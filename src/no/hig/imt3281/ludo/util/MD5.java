package no.hig.imt3281.ludo.util;

public class MD5 {
	/**
	 * Take a string, perform an md5 hash on it and return the hashed value as a string
	 * 
	 * @param md5 the string to perform the md5 hashing on
	 * @return the hash value of the input string
	 */
	public static String MD5(String md5) {
		try {
			java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
			byte[] array = md.digest(md5.getBytes());
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < array.length; ++i) {
				sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
			}
			return sb.toString();
		} catch (java.security.NoSuchAlgorithmException e) {
		}
		return null;
	}
}
