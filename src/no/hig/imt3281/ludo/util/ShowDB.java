package no.hig.imt3281.ludo.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

public class ShowDB {
	private final static String url = "jdbc:derby:ludoServer;create=true";
	
	public static void main(String[] args) {
		try {
			Connection con =  DriverManager.getConnection(url);
			Statement stmt = con.createStatement();
			// use execute to perform CREATE, INSERT, UPDATE, DELETE, executeQuery is for SELECT
			// MySQL has auto_increment, Derby has GENERATED values
			ResultSet res = stmt.executeQuery("SELECT * FROM users");
			ResultSetMetaData meta = res.getMetaData();
			// such as the number of column returned
			int numCols = meta.getColumnCount();
			System.out.println("Content of ludoServer::users");
			for (int i=0; i<numCols; i++)
				// The meta data also contains the names of the columns
				// NOTE!!!!! the index starts at 1 (ONE)
				System.out.printf("%-20s\t", meta.getColumnName(i+1));	// Indeksen starter på 1 (EN!!!)
			System.out.println();
			// The result set contains a pointer that is placed in front of the first row
			// we loop through the result as long as that pointer can be moved ahead
			while (res.next()) {
				// the while moves us through the rows of the result, now go through the columns
				for (int i=0; i<numCols; i++)
					// Write out every column 
					System.out.printf("%-20s\t", res.getObject(i+1));		// Indeksen starter på 1 (EN!!!)
				System.out.println();
			}

			con.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}