package no.hig.imt3281.ludo.server;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import javax.net.ssl.SSLServerSocketFactory;

import no.hig.imt3281.ludo.Logging;
import no.hig.imt3281.ludo.server.commands.*;
import i18n.I18N;

public class Server {
	private final static ResourceBundle resources = I18N.getServerBundle();
	private final static Logger logger = Logging.getLogger();
	private final static String url = "jdbc:derby:ludoServer";
	private static Connection database;
	private final static int loginPortNumber = 8989;
	private final static int masterPortNumber = 8990;
	private final static int channelsRequestPortNumber = 8988;
	private Vector<Client> clients = new Vector<>();
	private LinkedBlockingQueue<String> commandQueue = new LinkedBlockingQueue<>();
	private LinkedBlockingQueue<Command> commandDispatchQueue = new LinkedBlockingQueue();
	private 	ServerSocket loginServerSocket=null;
	private ServerSocket masterServerSocket=null;
	private Vector<Client> clientsWithIOError = new Vector<>();
	private Vector<Client> clientsToAdd = new Vector<>();
	private Vector<Client> waitingForRandomGame = new Vector<>();
	private Vector<Game> runningGames = new Vector<>();
	private final static String sslPwd = "P@ssw0rd!";
	private final static String sslFName = "/config/ssl.key";

	
	public Server () {
		try {
			logger.fine("Connecting to database.");
			database =  DriverManager.getConnection(url);
		} catch (SQLException sqle) {
			Logging.printStackTrace(sqle);
		}
		startLoginConnectionListener();
		startConnectionListener();
		startMessageListener();
		startCommandHandler();
		startCommandDispatcher();
		startRandomGameHandler();
	}
	
	private void startRandomGameHandler() {
		Thread t = new Thread(()->{
			while (true) {
				if (waitingForRandomGame.size()>3) {
					Game game = new Game(waitingForRandomGame.remove(0), waitingForRandomGame.remove(0),
															waitingForRandomGame.remove(0), waitingForRandomGame.remove(0));
					runningGames.add(game);
					game.setID(runningGames.indexOf(game));
					try {
						commandDispatchQueue.put(new StartNewGame(game));
						commandDispatchQueue.put(new GameActivePlayer(game));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		t.setDaemon(true);
		t.start();
	}

	private void startCommandDispatcher() {
		new Thread(()->{
			try {
				Command cmd;
				while ((cmd=commandDispatchQueue.take())!=null) {
					final Command tmpCmd = cmd;
					if (cmd instanceof GlobalChatCommand) {
						synchronized(clients) {
							logger.info("Dispatching: "+cmd+" to "+clients.size()+"clients");
							clients.stream().parallel().forEach(client->{
								try {
									client.write("MESSAGE\t-1\t"+((GlobalChatCommand)tmpCmd).getNickname()+"\t"+((GlobalChatCommand)tmpCmd).getMessage());
								} catch (Exception e) {
									clientsWithIOError .add(client);
								}
							});
						}
					} else if (cmd instanceof StartNewGame) {
						StartNewGame newGame = (StartNewGame)cmd;
						Client client=null;
						try {
							for (int i=0; i<4; i++) {
								client = newGame.getGame().getPlayer(i);
								logger.info(newGame.getGame().newGame());
								client.write(newGame.getGame().newGame());
							}
						} catch (IOException e) {
							clientsWithIOError .add(client);
						}
					} else if (cmd instanceof GameActivePlayer) {
						GameActivePlayer gameCmd = (GameActivePlayer)cmd;
						Client client=null;
						try {
							for (int i=0; i<4; i++) {
								client = gameCmd.getGame().getPlayer(i);
								if (gameCmd.getGame().getPlayer(gameCmd.getGame().getActivePlayer())==client)	
									client.write("PLAYER TURN\t"+gameCmd.getGame().getId()+"\t"+gameCmd.getGame().getActivePlayer()+"\tYOUR TURN");
								else
									client.write("PLAYER TURN\t"+gameCmd.getGame().getId()+"\t"+gameCmd.getGame().getActivePlayer());
							}
						} catch (IOException e) {
							clientsWithIOError .add(client);
						}
					} else if (cmd instanceof PassTheDice) {
						PassTheDice gameCmd = (PassTheDice)cmd;
						Game game = runningGames.get((int) gameCmd.getGameId());
						int player = (gameCmd.getPlayer()+1)%4;
						game.setActivePlayer(player);
						Client client=null;
						try {
							for (int i=0; i<4; i++) {
								client = game.getPlayer(i);
								if (game.getPlayer(game.getActivePlayer())==client)	
									client.write("PLAYER TURN\t"+game.getId()+"\t"+game.getActivePlayer()+"\tYOUR TURN");
								else
									client.write("PLAYER TURN\t"+game.getId()+"\t"+game.getActivePlayer());
							}
						} catch (IOException e) {
							clientsWithIOError .add(client);
						}
					} else if (cmd instanceof MovePiece) {
						MovePiece gameCmd = (MovePiece)cmd;
						Game game = runningGames.get((int) gameCmd.getGameId());
						Client client=null;
						try {
							for (int i=0; i<4; i++) {
								client = game.getPlayer(i);
								System.out.println (gameCmd);
								if (game.getPlayer(game.getActivePlayer())!=client)	
									client.write(gameCmd.toString());
							}
						} catch (IOException e) {
							clientsWithIOError .add(client);
						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();
	}

	private void startCommandHandler() {
		new Thread (()->{
			String msg;
			try {
				while ((msg=commandQueue.take())!=null) {
					logger.info("Command handler: "+msg);
					String command[] = msg.split("[\t]");
					if (command[2].equals("MESSAGE")) {
						if (command[3].equals("-1")) {
							commandDispatchQueue.put(new GlobalChatCommand (Integer.parseInt(command[0]), command[1], command[4]));
						}
					} else if (command[2].equals("PASS")) {
						commandDispatchQueue.put(new PassTheDice(Long.parseLong(command[3]), Integer.parseInt(command[4])));
					} else if (command[2].equals("MOVE")) {
						commandDispatchQueue.put(new MovePiece(Long.parseLong(command[3]), Integer.parseInt(command[4]),
																								Integer.parseInt(command[5]), Integer.parseInt(command[6])));
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();
	}

	private void startMessageListener() {
		new Thread (()->{
			while (true) {
				clients.stream().parallel().forEach(client -> {
					try {
						if (client.ready()) {
							String cmd = client.readLine();
							logger.info("Message received: "+cmd);
							if (cmd.equals("JOIN RANDOM GAME"))
								waitingForRandomGame.add(client);
							else
								commandQueue.put (client.getUId()+"\t"+client.getNickname()+"\t"+cmd);
						}
					} catch (Exception e) {
						Logging.printStackTrace(e);
					}
				});
				try {
					Thread.currentThread().sleep(100);
				} catch (Exception e) {
				}
				removeUnusedClients();
				addNewClients();
			}
		}).start();
	}

	private void addNewClients() {
		synchronized (clients) {
			while (clientsToAdd.size()>0)
				clients.add(clientsToAdd.remove(0));
		}
	}

	private void removeUnusedClients() {
		synchronized (clients) {
			clientsWithIOError.stream().parallel().forEach(client -> {
				Logging.getLogger().info("Removing client: "+client);
				clients.remove(client);	
			});
		}
		clientsWithIOError.clear();
	}

	public static Connection getDatabaseConnection () {
		return database;
	}
	
	private void startLoginConnectionListener() {
		// getClass().getResourceAsStream(sslFName)
		File f = new File ("./ssl.key");
		if (!f.exists()) {
			InputStream is = getClass().getResourceAsStream("/config/ssl.key");
			try {
				FileOutputStream fos = new FileOutputStream(f);
				byte buffer[] = new byte[1024*8];
				int bytesRead;
				while ((bytesRead = is.read(buffer))>-1) {
					fos.write(buffer, 0, bytesRead);
				}
				fos.close();
				is.close();
				Logging.getLogger().info("ssl.key file created outside of jar file");
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.setProperty("javax.net.ssl.keyStore", "ssl.key");
		System.setProperty("javax.net.ssl.keyStorePassword", sslPwd);

		try {
			SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
		    loginServerSocket = ssf.createServerSocket(loginPortNumber);
		} catch (IOException ioe) {
			Logging.printStackTrace(ioe);
			logger.severe("Unable to open port: "+loginPortNumber);
			System.exit(-1);
		}
		new Thread (() -> {	// Accept requests and add to client list
			Socket s;
			try {
				while ((s = loginServerSocket.accept()) != null) {
					try {
						Logging.getLogger().info("Connection accepted on login server socket");
						Client c = new Client (s);
					} catch (Exception e) {
						Logging.printStackTrace (e);
					}
				}
				loginServerSocket.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}).start();
	}

	private void startConnectionListener() {
		try {
			masterServerSocket = new ServerSocket (masterPortNumber);
			new Thread (() -> {	// Accept requests and add to client list
				Socket s;
				try {
					while ((s = masterServerSocket.accept()) != null) {
						try {
							Client c = new Client (s,true);
							synchronized(clientsToAdd) {
								clientsToAdd.add (c);
							}
						} catch (Exception e) {
							Logging.printStackTrace (e);
						}
					}
					masterServerSocket.close();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}).start();
		} catch (IOException e1) {
			e1.printStackTrace();
			System.exit(0);
		}

	}
	
	public static void main(String[] args) {
		new Server ();
	}
}
