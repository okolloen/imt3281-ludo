package no.hig.imt3281.ludo.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Logger;

import no.hig.imt3281.ludo.Logging;
import no.hig.imt3281.ludo.util.MD5;

/**
 * Class used to represent a client in the server.
 * Holds information about the socket, the input/output readers and 
 * user id/nickname.
 * 
 * @author oivindk
 *
 */
public class Client {
	private Socket socket;
	private BufferedReader input;
	private BufferedWriter output;
	private String nickname = "";
	private int uid = -1;
	
	/**
	 * Constructor for the client class.
	 * Takes a socket object as parameter, establish reader/writer objects on the socket and reads 
	 * initial command from the client.
	 * The initial command is a tab delimited string.
	 * The first word is "login" or "register". 
	 * 		"LOGIN" : expects the next words to be username and password
	 * 		"REGISTER" : expects the next words to be username, password, given name, surename, nickname
	 * 
	 * Throws an exception if any errors. 
	 * 
	 * @param s the socket that holds the client connection
	 * @throws Exception indicating some error, i.e. Unable to open streams, database error, unknown user, duplicate username
	 */
	public Client (Socket s) throws Exception {
		socket = s;
		try {
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			String args[] = readLine().split("[\t]");
			Logging.getLogger().info(Arrays.toString(args));
			if (args[0].equals("LOGIN")) {
				login (args, s.getInetAddress().getHostAddress());
			} else if (args[0].equals("REGISTER")) {
				register (args);
				write ("REGISTER OK");
			} else if (args[0].equals("EMERGENCY SHUTDOWN")) {		// Make it possible to shut down the server remotely
				Logging.getLogger().severe("EMERGENCY SHUTDOWN RECEIVED");
				System.exit (0);
			} else {
				write ("Unknown command");
				throw new Exception ("Unknown command from client");
			}
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception ("Unable to open stream from client");
		}
	}

	public Client(Socket s, boolean b) throws Exception {
		socket = s;
		try {
			input = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			String key = readLine();
			Logging.getLogger().info(key);
			loginWithKey (key, s.getInetAddress().getHostAddress());
		} catch (IOException e) {
			e.printStackTrace();
			throw new Exception ("Unable to open stream from client");
		}
	}

	/**
	 * Method to return uid for user.
	 * Return -1 if user not logged in, should never happen as no user object should be 
	 * created if no user with given credentials exists/can be created.
	 * 
	 * @return the user id for this user.
	 */
	public int getUId() {
		return uid;
	}
	/**
	 * Method to return the nickname for this user.
	 * 
	 * @return a String with the nickname for the user.
	 */
	public String getNickname () {
		return nickname;
	}
	
	/**
	 * Method used to write a string back to the client, including newline and flushing.
	 * 
	 * @param s the string to be written to the client.
	 * @throws IOException if an error occurs
	 */
	public void write(String s) throws IOException {
		output.write(s);
		output.newLine();
		output.flush();
	}
	
	/**
	 * @see java.io.BufferedReader.ready
	 */
	public boolean ready () throws IOException { 
		return input.ready ();
	}
	
	/**
	 * @see java.io.BufferedReader.readLine
	 */
	public String readLine () throws IOException {
		return input.readLine();
	}
	
	/*
	 * Method used to find a user from the user table given username and password.
	 * If a user is found the userid and nickname is set.
	 * 
	 * @throws Exception if database error or user not found
	 */
	private void login(String[] args, String hostname) throws Exception {
		try {
			PreparedStatement stmnt = Server.getDatabaseConnection()
					.prepareStatement("SELECT id, nickname FROM users WHERE email=? and password=?");
			stmnt.setString(1, args[1]);
			stmnt.setString(2, args[2]);
			ResultSet result = stmnt.executeQuery ();
			if (!result.next()) {
				write("Uknown username/password");
				throw new Exception ("Unknown username/password!");
			}
			uid = result.getInt(1);
			nickname = result.getString(2);
			stmnt.close();
			Date now = new Date();
			String key = MD5.MD5(uid+nickname+now.toString());
			stmnt = Server.getDatabaseConnection()
					.prepareStatement("UPDATE users SET logins=logins+1, loginkey=?, loginhost=?, lastLogin=CURRENT_TIMESTAMP WHERE id=?");
			stmnt.setInt(3, uid);
			stmnt.setString(1, key);
			stmnt.setString(2, hostname);
			stmnt.execute();
			write ("LOGIN OK\t"+key);
		} catch (SQLException sqle) {
			write("Database error");
			throw new Exception ("Unable to perform database query.");
		}
	}
	
	/*
	 * Method used to find a user from the user table given username and password.
	 * If a user is found the userid and nickname is set.
	 * 
	 * @throws Exception if database error or user not found
	 */
	private void loginWithKey (String key, String hostname) throws Exception {
		try {
			PreparedStatement stmnt = Server.getDatabaseConnection()
					.prepareStatement("SELECT id, nickname FROM users WHERE loginkey=? and loginhost=?");
			stmnt.setString(1, key);
			stmnt.setString(2, hostname);
			ResultSet result = stmnt.executeQuery ();
			if (!result.next()) {
				write("Uknown username/password");
				throw new Exception ("Unknown username/password!");
			}
			uid = result.getInt(1);
			nickname = result.getString(2);
			stmnt.close();
			stmnt = Server.getDatabaseConnection()
					.prepareStatement("UPDATE users SET logins=logins+1, loginkey='', loginhost='', lastLogin=CURRENT_TIMESTAMP WHERE id=?");
			stmnt.setInt(1, uid);
			stmnt.execute();
			write ("LOGIN OK\t"+key);
		} catch (SQLException sqle) {
			write("Database error");
			throw new Exception ("Unable to perform database query.");
		}
	}
	
	/*
	 * Method used register a new user in the database and log that user in.
	 * Throws an exception if the username is taken.
	 * 
	 * @throws Exception if database error or username is taken.
	 */
	private void register(String[] args) throws Exception {
		try {
			PreparedStatement stmnt = Server.getDatabaseConnection()
					.prepareStatement("insert into users (email, password, givenname, surename, nickname, logins, lastLogin) "+
												"VALUES (?, ?, ?, ?, ?, 0, '1980-01-01 00:00:00')");
			stmnt.setString(1, args[1]); // email/username
			stmnt.setString(2, args[2]); // password
			stmnt.setString(3, args[3]); // given name
			stmnt.setString(4, args[4]); // sure name
			stmnt.setString(5, args[5]); // nickname
			int result = stmnt.executeUpdate ();
			if (result<1) {
				write("Username is occupied");
				throw new Exception ("Username is occupied");
			}
		} catch (SQLException sqle) {
			write("Database error/Username is occupied");
			throw new Exception ("Unable to perform database query.");
		}
	}
}
