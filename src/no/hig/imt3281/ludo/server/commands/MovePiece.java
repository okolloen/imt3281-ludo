package no.hig.imt3281.ludo.server.commands;

public class MovePiece extends Command {

	private long gameid;
	private int player;
	private int piece;
	private int targetpos;

	public MovePiece(long gameid, int player, int piece, int targetPos) {
		super (-1, null);
		this.gameid = gameid;
		this.player = player;
		this.piece = piece;
		this.targetpos = targetPos;
	}
	
	public String toString () {
		return "MOVE\t"+gameid+"\t"+player+"\t"+piece+"\t"+targetpos;
	}

	public long getGameId() {
		return gameid;
	}
}
