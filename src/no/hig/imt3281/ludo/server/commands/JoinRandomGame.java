package no.hig.imt3281.ludo.server.commands;

/**
 * Objects of this class on the command queue means this client want to join a running game.
 * 
 * @author oivindk
 *
 */
public class JoinRandomGame extends Command {

	public JoinRandomGame(int uid, String nickname) {
		super(uid, nickname);
	}
}
