package no.hig.imt3281.ludo.server.commands;

public class Command {
	private int uid;
	private String nickname;
	
	public Command (int uid, String nickname) {
		this.uid = uid;
		this.nickname = nickname;
	}

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the nickname
	 */
	public String getNickname() {
		return nickname;
	}

	/**
	 * @param nickname the nickname to set
	 */
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
}
