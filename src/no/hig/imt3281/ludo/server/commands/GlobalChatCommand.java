package no.hig.imt3281.ludo.server.commands;

public class GlobalChatCommand extends Command {
	String message;
	
	public GlobalChatCommand (int uid, String nickname, String message) {
		super(uid, nickname);
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
}
