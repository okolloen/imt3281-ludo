package no.hig.imt3281.ludo.server.commands;

import no.hig.imt3281.ludo.server.Game;

public class StartNewGame extends Command {
	Game game;
	
	public StartNewGame(Game game) {
		super(-1, null);
		this.game = game;
	}
		
	public Game getGame() {
		return game;
	}
	
}
