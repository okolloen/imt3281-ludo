package no.hig.imt3281.ludo.server.commands;

import no.hig.imt3281.ludo.server.Game;

public class GameActivePlayer extends Command {
	Game game;
	
	public GameActivePlayer(Game game) {
		super(-1, null);
		this.game = game;
	}
	
	public Game getGame() {
		return game;
	}
}
