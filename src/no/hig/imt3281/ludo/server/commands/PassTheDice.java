package no.hig.imt3281.ludo.server.commands;

import no.hig.imt3281.ludo.server.commands.Command;

public class PassTheDice extends Command {
	long gameid;
	int player;

	public PassTheDice(long id, int player) {
		super (-1, null);
		this.gameid = id;
		this.player = player;
	}

	/**
	 * @return the gameid
	 */
	public long getGameId() {
		return gameid;
	}

	/**
	 * @return the player
	 */
	public int getPlayer() {
		return player;
	}

}
