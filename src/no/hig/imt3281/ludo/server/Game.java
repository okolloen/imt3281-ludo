package no.hig.imt3281.ludo.server;

import no.hig.imt3281.ludo.client.LudoBoard;

public class Game {
	Client player[] = new Client[4];
	LudoBoard.LudoLogic board = new LudoBoard.LudoLogic();
	String name = "Random game";
	long id;
	int activePlayer=0;

	public Game(Client p1, Client p2, Client p3, Client p4) {
		player[0] = p1;
		player[1] = p2;
		player[2] = p3;
		player[3] = p4;
	}
	
	public String newGame() {
		return "NEW GAME\t"+id+"\t"+name+"\t"+player[0].getUId()+"\t"+player[0].getNickname()+"\t"+
				player[1].getUId()+"\t"+player[1].getNickname()+"\t"+
				player[2].getUId()+"\t"+player[2].getNickname()+"\t"+
				player[3].getUId()+"\t"+player[3].getNickname();
	}
	
	public void setID(long id) {
		this.id = id;
	}
	
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @return the player1
	 */
	public Client getPlayer(int player) {
		return this.player[player];
	}

	public String getName() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public int getActivePlayer() {
		return activePlayer;
	}
	
	public void setActivePlayer(int player) {
		activePlayer = player;
	}
}
