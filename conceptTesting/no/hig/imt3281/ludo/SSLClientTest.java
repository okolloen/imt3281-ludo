package no.hig.imt3281.ludo;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.Socket;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManagerFactory;

public class SSLClientTest {
  public static void main(String args[]) throws Exception {
/*	System.setProperty("javax.net.ssl.keyStore", "ssl.key");
	System.setProperty("javax.net.ssl.keyStorePassword", "P@ssw0rd!");

    SSLSocketFactory ssf = (SSLSocketFactory) SSLSocketFactory.getDefault();
    Socket s = ssf.createSocket("127.0.0.1", 5432); */
	//passphrase for keystore
	  char[] keystorePass="P@ssw0rd!".toCharArray();

	  //load own keystore (MyApp just holds reference to application context)
	  KeyStore keyStore=KeyStore.getInstance(KeyStore.getDefaultType());
	  keyStore.load(new java.io.FileInputStream("ssl.key"),keystorePass);

	  //create a factory
	  TrustManagerFactory     trustManagerFactory=TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
	  trustManagerFactory.init(keyStore);

	  //get context
	  SSLContext sslContext=SSLContext.getInstance("TLS");

	  //init context
	  sslContext.init(
	      null,
	      trustManagerFactory.getTrustManagers(), 
	      new SecureRandom()
	  );

	  //create the socket
	  Socket s=sslContext.getSocketFactory().createSocket("127.0.0.1",5432);
	  s.setKeepAlive(true);

    SSLSession session = ((SSLSocket) s).getSession();
    Certificate[] cchain = session.getPeerCertificates();
    System.out.println("The Certificates used by peer");
    for (int i = 0; i < cchain.length; i++) {
      System.out.println(((X509Certificate) cchain[i]).getSubjectDN());
    }
    System.out.println("Peer host is " + session.getPeerHost());
    System.out.println("Cipher is " + session.getCipherSuite());
    System.out.println("Protocol is " + session.getProtocol());
    System.out.println("ID is " + new BigInteger(session.getId()));
    System.out.println("Session created in " + session.getCreationTime());
    System.out.println("Session accessed in " + session.getLastAccessedTime());

    BufferedReader in = new BufferedReader(new InputStreamReader(s.getInputStream()));
    String x = in.readLine();
    System.out.println(x);
    in.close();

  }
}