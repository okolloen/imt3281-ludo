package no.hig.imt3281.ludo;

import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

import javax.net.ssl.SSLServerSocketFactory;

public class SSLServerTest {
  public static void main(String args[]) throws Exception {
	System.setProperty("javax.net.ssl.keyStore", "ssl.key");
	System.setProperty("javax.net.ssl.keyStorePassword", "P@ssw0rd!");
    SSLServerSocketFactory ssf = (SSLServerSocketFactory) SSLServerSocketFactory.getDefault();
    ServerSocket ss = ssf.createServerSocket(5432);
    while (true) {
      Socket s = ss.accept();
      PrintStream out = new PrintStream(s.getOutputStream());
      out.println("Hi");
      out.close();
      s.close();
    }

  }
}