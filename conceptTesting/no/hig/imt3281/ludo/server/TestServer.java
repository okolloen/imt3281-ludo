package no.hig.imt3281.ludo.server;

import java.awt.BorderLayout;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.LinkedTransferQueue;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.BevelBorder;

@SuppressWarnings("serial")
public class TestServer extends JFrame {
	JList<String> clients = new JList<>();
	JTextArea messages = new JTextArea();
	Vector<Client> connected = new Vector<>();
	LinkedTransferQueue<String> commands = new LinkedTransferQueue<>();

	public TestServer ()  {
		try {
			initConnectionAcceptorThread();
			initReaderThread();
			initSenderThread();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		JPanel clientsPanel = new JPanel (new BorderLayout());
		clientsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED), "Connected clients"));
		clientsPanel.add(new JScrollPane(clients));
		add (clientsPanel, BorderLayout.EAST);
		JPanel main = new JPanel(new BorderLayout());
		main.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED), "Messages"));
		main.add (new JScrollPane(messages));
		add (main, BorderLayout.CENTER);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setSize (700, 500);
		setVisible (true);
	}

	private void initSenderThread() {
		new Thread(() -> {
			while (true) {
				try {
					String command = commands.take();
					connected.stream().forEach(c -> {							
						try {
							c.output.write (command);
							c.output.newLine();
							c.output.flush();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				} catch (InterruptedException ie) {
					ie.printStackTrace();
				}
			}
		}).start();
	}

	private void initConnectionAcceptorThread() throws IOException {
		ServerSocket ss = new ServerSocket (15678);
		Runnable r = () -> {	// Accept requests and add to client list
			Socket s;
			try {
				while ((s = ss.accept()) != null) {
					Client c = new Client (s);
					synchronized(connected) {
						connected.add(c);
					}
					messages.append(s.getInetAddress().toString()+":"+c.name+" -> connected\n");
				}
				ss.close();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		};
		new Thread(r).start();
	}

	private void initReaderThread() {
		new Thread(() -> {
			while (true) {
				try {
					Thread.sleep (100);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				synchronized(connected) {
					connected.stream().filter(c -> {
						try {
							return c.input.ready();
						} catch (Exception e) {
							return false;
						}}
					).forEach(c -> {							
						try {
							String tmp = c.input.readLine();
							messages.append(c.name+":"+tmp+"\n");
							commands.add (c.name+":"+tmp);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					});
				}
			}
		}).start();
	}
	
	public static void main(String[] args) {
		new TestServer();
	}

	class Client {
		Socket s;
		BufferedReader input;
		BufferedWriter output;
		String name;
		
		public Client (Socket s) {
			this.s = s;
			try {
				output = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
				input = new BufferedReader(new InputStreamReader(s.getInputStream()));
				name = input.readLine();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
