package no.hig.imt3281.ludo.client;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MultiTestClient {

	public MultiTestClient () {
		ExecutorService executor = Executors.newFixedThreadPool(100);
		Client clients[] = new Client[1000];
		for (int i=0; i<clients.length; i++)
			clients[i] = new Client ("#"+i, executor);
		clients[15].send("Hallo");
		clients[36].send("Hallo");
		clients[42].send("Hallo");
		clients[76].send("Hallo");
		Scanner in = new Scanner(System.in);
		in.next();
		clients[87].send("Hallo");
	}
	
	public static void main(String[] args) {
		new MultiTestClient();
	}

	class Client {
		String name;
		Socket s;
		BufferedReader input;
		BufferedWriter output;
		
		public Client (String name, ExecutorService executor) {
			try {
				s = new Socket ("127.0.0.1", 15678);
				input = new BufferedReader(new InputStreamReader(s.getInputStream()));
				output = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
				output.write(name);
				output.newLine();
				output.flush();
			} catch (UnknownHostException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			this.name = name;
			executor.submit(() -> {	// Read strings from the server
				try {
					while (true) {
						System.out.println (name+"->"+input.readLine());
					}
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			});
		}
		
		public void send (String tmp) {
			try {
				output.write(tmp);
				output.newLine();
				output.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
