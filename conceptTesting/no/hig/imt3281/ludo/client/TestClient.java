package no.hig.imt3281.ludo.client;

import java.awt.BorderLayout;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.BevelBorder;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;

@SuppressWarnings("serial")
public class TestClient extends JFrame {
	JTabbedPane clientsPane = new JTabbedPane();

	public TestClient () {
		super ("Test clients for IMT3281 Ludo project");
		
		// Setup GUI
		JMenuBar menu = new JMenuBar();
		JMenu clients = new JMenu("Clients");
		menu.add(clients);
		JMenuItem newClient = new JMenuItem("New client");
		clients.add(newClient);
		setJMenuBar(menu);
		add (clientsPane);
		
		// Add action listeners
		newClient.addActionListener(e -> {
			String name = JOptionPane.showInputDialog(this, "Name of client");
			clientsPane.addTab(name, new Client(name));
		});
		
		// Set size and close operation
		setSize (900, 700);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible (true);
	}
	
	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException, UnsupportedLookAndFeelException {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		new TestClient ();
	}

	@SuppressWarnings("serial")
	class Client extends JPanel {
		JTextPane received = new JTextPane();
		StyledDocument doc = received.getStyledDocument();
		SimpleAttributeSet style = new SimpleAttributeSet();
		JTextField send = new JTextField();
		String name;
		Socket s;
		BufferedReader input;
		BufferedWriter output;
		
		public Client (String name) {
			try {
				s = new Socket ("127.0.0.1", 15678);
				input = new BufferedReader(new InputStreamReader(s.getInputStream()));
				output = new BufferedWriter(new OutputStreamWriter(s.getOutputStream()));
				output.write(name);
				output.newLine();
				output.flush();
			} catch (UnknownHostException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			} catch (IOException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			this.name = name;
			setLayout(new BorderLayout());
			JPanel receivedPanel = new JPanel(new BorderLayout());
			receivedPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED), "Incoming"));
			receivedPanel.add(received);
			add (receivedPanel, BorderLayout.CENTER);
			JPanel sendPanel = new JPanel(new BorderLayout());
			sendPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED), "Send"));
			sendPanel.add(send);
			send.addActionListener(e -> {
				try {
					output.write(send.getText());
					output.newLine();
					output.flush();
					send.setText("");
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			});
			add (sendPanel, BorderLayout.SOUTH);
			Runnable r = () -> {	// Read strings from the server
				try {
					while (true) {
						doc.insertString (doc.getLength(), input.readLine()+"\n", style);
					}
				} catch (BadLocationException e1) {
					e1.printStackTrace();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			};
			new Thread(r).start();
		}
	}
}
